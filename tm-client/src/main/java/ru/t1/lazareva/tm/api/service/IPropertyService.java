package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

}
