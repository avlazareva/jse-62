package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.service.dto.IDtoService;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.repository.dto.AbstractDtoRepository;

import java.util.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractModelDto, R extends AbstractDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected R repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        resultModel = repository.save(model);
        return resultModel;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return repository.saveAll(models);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Optional<M> resultModel;
        resultModel = repository.findById(id);
        return resultModel.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public int getSize() throws Exception {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @Nullable M resultModel;
        return repository.save(model);
    }

}
