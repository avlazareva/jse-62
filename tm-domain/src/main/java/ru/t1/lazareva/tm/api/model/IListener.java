package ru.t1.lazareva.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IListener {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @NotNull
    String getName();

    void execute();

}